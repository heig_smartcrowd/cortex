/**
 * New Relic agent configuration.
 *
 * See lib/config.defaults.js in the agent distribution for a more complete
 * description of configuration variables and their potential values.
 */
exports.config = {
  /**
   * Array of application names.
   */
  app_name : ['SmartCrowd-Cortex'],
  /**
   * Your New Relic license key.
   */
  license_key : '516c14c87e3914540198950cc78154c985470d87',
  // Enable by settings environment variable NEW_RELIC_ENABLED=false
  agent_enabled: false,
  logging : {
    /**
     * Level at which to log. 'trace' is most useful to New Relic when diagnosing
     * issues with the agent, 'info' and higher will impose the least overhead on
     * production applications.
     */
    level : 'info'
  }
};
