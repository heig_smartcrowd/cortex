var db = require('../db');

exports.setStaff = function(req, res) {
  var staff = req.body.staff == "true";
  db.setStaff(req.params['phone_id'], staff, function(err, result) {
    if (err) {
      throw(err);
    }
    res.send(200);
  });
}

exports.phonesList = function(req, res) {
  db.ids(function(items) {
    res.render('phones_list', {
      title: 'Phones list',
      items: items
    });
  });
}

exports.viewPhone = function(req, res) {
  var phone_id = req.params.phone_id;
  res.render('phone_map', {
    title: 'Phone ' + phone_id,
    phone_id: req.params.phone_id,
  });
}

exports.history = function(req, res) {
  res.render('history', {
    title: 'History'
  });
}

exports.latests = function(req, res) {
  res.render('latests', {
    title: 'Latests'
  });
}
