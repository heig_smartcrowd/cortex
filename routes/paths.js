var db = require('../db');

// For each device, returns a GeoJSON containing a MultiPoint feature
// containing the last N positions of the device
exports.lastN = function(req, res) {
  db.lastN(5, function(items) {
    // Convert to geojson
    var features = [];
    for (var i = 0; i < items.length; i++) {
      var phone_id = items[i].phone_id;
      var locations = items[i].locations;
      var out_timestamps = []
      var out_coords = [];
      for (var j = 0; j < locations.length; j++) {
        out_coords.push([locations[j].longitude, locations[j].latitude]);
        out_timestamps.push(locations[j].phone_timestamp);
      }
      features.push({
        "type" : "Feature",
        "properties": {
          "phone_id" : phone_id,
          "timestamps" : out_timestamps
        },
        "geometry" : {
          "type" : "MultiPoint",
          "coordinates" : out_coords,
        }
      });
    }
    var geojson = {
      "type" : "FeatureCollection",
      "features" : features
    };
    res.send(geojson);
  });
}

// For each device, returns a GeoJSON containing a single Point feature
// containing the last position sent by the device
exports.lastPositions = function(req, res) {
  db.lastN(1, function(items) {
    // Convert to GeoJSON
    var features = [];
    for (var i = 0; i < items.length; i++) {
      var phone_id = items[i].phone_id;
      if (items[i].locations.length != 1) {
        throw "Item with locations.length != 1 for phone_id " + phone_id
      }
      var out_timestamp = items[i].locations[0].phone_timestamp;
      var out_coords = [items[i].locations[0].longitude,
                        items[i].locations[0].latitude];
      features.push({
        "type" : "Feature",
        "properties": {
          "phone_id" : phone_id,
          "timestamp" : out_timestamp,
        },
        "geometry" : {
          "type" : "Point",
          "coordinates" : out_coords,
        }
      });
    }
    var geojson = {
      "type" : "FeatureCollection",
      "features" : features
    };
    res.send(geojson);
  });
}

exports.positionAt = function(req, res) {
  console.log(req.query);
  var start = parseFloat(req.query.start);
  var end = parseFloat(req.query.end);
  db.positionAt(start, end, function(items) {
    // Conver to GeoJSON
    var features = [];
    for (var i = 0; i < items.length; i++) {
      var phone_id = items[i].phone_id;
      if (items[i].locations.length != 1) {
        throw "Item with locations.length != 1 for phone_id " + phone_id
      }
      var out_timestamp = items[i].locations[0].phone_timestamp;
      var out_coords = [items[i].locations[0].longitude,
                        items[i].locations[0].latitude];
      features.push({
        "type" : "Feature",
        "properties": {
          "phone_id" : phone_id,
          "timestamp" : out_timestamp,
        },
        "geometry" : {
          "type" : "Point",
          "coordinates" : out_coords,
        }
      });
    }
    var geojson = {
      "type" : "FeatureCollection",
      "features" : features
    };
    res.send(geojson);
  });
}


