// Transform a list of items directly from the database to GeoJSON
// Assumes the items are sorted
exports.locationsToGeoJSON = function(items) {
  var coords = [];
  var timestamps = [];
  for (var i = 0; i < items.length; i++) {
    coords.push([items[i].longitude, items[i].latitude]);
    timestamps.push(items[i].phone_timestamp);
  }

  var feature = {
    "type" : "Feature",
    "properties" : {
      "phone_id" : items[0].phone_id,
      "timestamps" : timestamps
    },
    "geometry": {
      "type" : "MultiPoint",
      "coordinates" : coords
    }
  };
  var geojson = {
    "type" : "FeatureCollection",
    "features" : [feature]
  };
  return geojson;

}
