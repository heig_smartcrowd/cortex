var express = require('express');
var http = require('http');
var MongoClient = require('mongodb').MongoClient;

var paths = require('./routes/paths');
var stats = require('./routes/stats');
var viewer = require('./routes/viewer');
var db = require('./db');
var utils = require('./utils');

var app = express();

app.configure(function() {
  app.use(express.bodyParser());

  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');

  // http://stackoverflow.com/questions/10434001/static-files-with-express-js
  app.use('/static', express.static(__dirname + '/static'));
  //app.use('/viewer', express.static(__dirname + '/public'));
});

app.configure('development', function() {
  console.log('Using development environment');
  app.use(express.logger('dev'));
  app.set('port', 4000);
});

app.configure('production', function() {
  console.log('Using production environment');
  app.use(express.logger('dev'));
  console.log('$PORT = ' + process.env.PORT);
  if (! process.env.NEW_RELIC_APP_NAME) {
    throw('Environment variable NEW_RELIC_APP_NAME must be defined for prod');
  }
  app.set('port', process.env.PORT || 4000);

});

app.get('/', function(req, res) {
  res.redirect('/viewer/phone');
  //res.render('index', {});
});

app.get('/paths', paths.lastN);
app.get('/last_positions', paths.lastPositions);
app.get('/timerange', stats.timerange);
// Given a time range, will return, for each device, its most recent position
// in said time range.
app.get('/position_at', paths.positionAt);

app.get('/viewer/history', viewer.history);
app.get('/viewer/latests', viewer.latests);
app.get('/viewer/phone', viewer.phonesList);
app.get('/viewer/phone/:phone_id', viewer.viewPhone);

// Edit staff status
app.post('/setstaff/:phone_id', viewer.setStaff);

// Return all the data we have for the phone
app.get('/phone/:phone_id/all', function(req, res) {
  db.phone(req.params.phone_id, function(items) {
    res.send(items);
  });
});

// Return location only as a geojson
app.get('/phone/:phone_id/geojson', function(req, res) {
  db.phoneLocations(req.params.phone_id, function(items) {
    res.send(utils.locationsToGeoJSON(items));
  });
});

app.set('mongodb_connection_string',
        'mongodb://localhost:27017/smartcrowd?journal=true&w=1')

// Override the original expressjs app.listen to setup DB before starting
// the http server
app.real_listen = app.listen;

// cb(server) is a callback that will receive the listening http server once
// it has been started
app.listen = function(cb) {
  var connString = app.get('mongodb_connection_string');
  var server = null;
  MongoClient.connect(connString, {
      server: {
        auto_reconnect: true
      }
    }, function(err, database) {
      if (err) {
        throw(err);
      }

      app.db = database;
      db.onDBConnected(database);

      // Start application once database connection is ready
      var port = app.get('port');
      console.log("Listening on port " + port);
      var server = app.real_listen(port);
      if (cb) {
        cb(server);
      }
    }
  );
}

exports.app = app;
