//var map = new L.Map("map_container", {center : [37.8, -96.9], zoom: 4})
    //.addLayer(new L.TileLayer("http://{s}.tiles.mapbox.com/v3/examples.map-vyofok3q/{z}/{x}/{y}.png"));

// -- Leaflet for map background (OSM) display
var map = new L.Map("map_container").setView([51.505, -0.09], 13);
L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

// -- d3 stuff
// http://bost.ocks.org/mike/leaflet/
var svg = d3.select(map.getPanes().overlayPane).append("svg"),
    g = svg.append("g").attr("class", "leaflet-zoom-hide");

console.log(svg);

// Load map bounds
d3.json("/static/heig.geojson", function(collection) {
  var geojson = L.geoJson(collection);
  map.fitBounds(geojson.getBounds());
});

function refreshPositions (cb) {
  console.log('Refreshing');
  // Load initial data
  d3.json("/paths", function(collection) {
    var transform = d3.geo.transform({point: streamProject}),
        path = d3.geo.path().projection(transform);

    //console.log(collection.features);
    var feature = g.selectAll("path")
        .data(collection.features);
    feature.enter().append("path");
    feature.exit().remove();

    map.on("viewreset", reset);
    reset();

    // Reposition the SVG on map zoom / pan
    function reset() {
      var bounds = path.bounds(collection),
          topLeft = bounds[0],
          bottomRight = bounds[1];

      // Adds a margin. Not quite sure if this is the correct way
      topLeft[0] -= 50;
      topLeft[1] -= 50;
      bottomRight[0] += 50;
      bottomRight[1] += 50;

      svg.attr("width", bottomRight[0] - topLeft[0])
         .attr("height", bottomRight[1] - topLeft[1])
         .style("left", topLeft[0] + "px")
         .style("top", topLeft[1] + "px");
      g.attr("transform", "translate(" + -topLeft[0] + "," + -topLeft[1] + ")");

      feature.attr("d", function(data) {
        //console.log(data);
        var line = d3.svg.line()
          .x(function(d, i) {
            d.projectedCoords = projectPoint(d[0], d[1]);
            return d.projectedCoords.x;
          })
          .y(function(d) {
            return d.projectedCoords.y;
          });
        return line(data.geometry.coordinates);
      });

      //feature.attr("d", path);
    }

    // Use leaflet to transform points
    function projectPoint(x, y) {
      var point = map.latLngToLayerPoint(new L.LatLng(y, x));
      return point;
    }

    function streamProject(x, y) {
      var point = projectPoint(x, y);
      this.stream.point(point.x, point.y);
    }

    cb();
  });
}

// Refresh every 5 seconds
//refreshPositions(function(){});
(function poll() {
  refreshPositions(function() {
    setTimeout(poll, 1000);
  });
})();

// TODO: Use websockets to receive updates
//var socket = io.connect('http://' + window.location.host);
//socket.on('news', function(data) {
  //console.log(data);
  //socket.emit('my other event', {my : 'data'});
//});


