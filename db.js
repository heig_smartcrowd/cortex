var redis = require('redis')
  , async = require('async');
var client = redis.createClient();

var db = null;

exports.onDBConnected = function(database) {
  db = database;
}


// Returns, for each phone_id, the last N locations
exports.lastN = function(N, cb) {
  db.collection('readings', function(err, collection) {
    collection.distinct('phone_id', function(err, phone_ids) {
      if(err) {
        throw(err);
      }
      async.map(phone_ids, function(phone_id, emit_cb) {
        // For each phone_id, queries the last N locations
        var fields = {
            _id : 0,
            latitude: 1,
            longitude: 1,
            phone_timestamp: 1
        };
        collection.find({ phone_id : phone_id, type: "location"}, fields)
                  .sort({'phone_timestamp' : -1})
                  .limit(N)
                  .toArray(function(err, items) {
          if (err) {
            throw(err);
          }
          var outval = {
            'phone_id' : phone_id,
            'locations' : items
          };
          emit_cb(null, outval);
        });
      }, function(err, results) {
        // Called once all mappers have finished
        cb(results);
      });
    });
  });
}

exports.positionAt = function(start, end, cb) {
  db.collection('readings', function(err, collection) {
    collection.distinct('phone_id', function(err, phone_ids) {
      if(err) {
        throw(err);
      }
      async.map(phone_ids, function(phone_id, emit_cb) {
        // For each phone_id, queries the most recent location in the interval
        var fields = {
            _id : 0,
            latitude: 1,
            longitude: 1,
            phone_timestamp: 1
        };
        collection.find({ phone_id : phone_id,
                          type: "location",
                          phone_timestamp: {
                            $gte: start,
                            $lte: end
                          }}, fields)
                  .sort({'phone_timestamp' : -1})
                  .limit(1)
                  .toArray(function(err, items) {
          if (err) {
            throw(err);
          }
          var outval = {
            'phone_id' : phone_id,
            'locations' : items
          };
          emit_cb(null, outval);
        });
      }, function(err, results) {
        // Called once all mappers have finished

        // Ignore devices with no location in the given range
        var valids = [];
        for (var i = 0; i < results.length; ++i) {
          if (results[i].locations.length > 0) {
            valids.push(results[i]);
          } else {
            //console.log('no location for ' + i);
          }
        }
        cb(valids);
      });
    });
  });
}

exports.timerange = function(cb) {
  db.collection('readings', function(err, collection) {
    collection.aggregate([
        { $match : {type:'location'} },
        { $group : {
          _id : 0,
          min_timestamp: { $min : "$phone_timestamp" },
          max_timestamp: { $max : "$phone_timestamp" }
        }}
      ], function(err, results) {
        cb({
          min_timestamp : results[0]['min_timestamp'],
          max_timestamp : results[0]['max_timestamp']
        });
      });
  });
}

exports.setStaff = function(phone_id, staff, cb) {
  if (staff) {
    client.sadd('staff', phone_id, function(err, res) {
      cb(err);
    });
  } else {
    client.srem('staff', phone_id, function(err, res) {
      cb(err);
    });
  }
}

exports.ids = function(cb) {
  // transforms phone_id to {"phone_id":<phone_id, "staff":<true|false>}
  function addIsStaff(phone_id, cb) {
    client.sismember('staff', phone_id, function(err, staff) {
      if (err) {
        cb(err);
        return;
      }
      data = {
        'phone_id' : phone_id,
        'staff' : staff === 1
      };
      cb(null, data);
    });
  }

  db.collection('readings', function(err, collection) {
    collection.distinct('phone_id', function(err, phones_ids) {
      if (err) {
        throw(err);
      }
      client.smembers('staff', function(err, members) {
        if (err) {
          throw(err);
        }
        // Build a set of staff members
        var staffs = {};
        for (var i = 0; i < members.length; ++i) {
          staffs[members[i]] = 1;
        }
        var outphones = [];
        for (var i = 0; i < phones_ids.length; ++i) {
          outphones.push({
            'phone_id' : phones_ids[i],
            'staff' : phones_ids[i] in staffs
          });
        }
        cb(outphones);
      });
    })
  });
}

exports.phone = function(phone_id, cb) {
  db.collection('readings', function(err, collection) {
    collection.find({'phone_id' : phone_id})
              .sort({'phone_timestamp' : -1})
              .toArray(function(err, items) {
      cb(items);
    });
  });
}

exports.phoneLocations = function(phone_id, cb) {
  db.collection('readings', function(err, collection) {
    collection.find({'phone_id' : phone_id, 'type' : 'location'})
              .sort({'phone_timestamp' : -1})
              .toArray(function(err, items) {
      cb(items);
    });
  });
}
