SmartCrowd "Cortex" analyzer
============================

The goal is to provide a HTML5-based map to show the data collected by
smartcrowd.

The cortex consists of a nodejs server that does the interface between the
web interface and the DB and of a pure frontend part that consist of the
HTML5 visualization.


References
----------
For map visualization using d3 :
http://bl.ocks.org/mbostock/5616813
https://github.com/d3/d3-plugins/tree/master/geo/tile

Leaflet + d3 :
http://bost.ocks.org/mike/leaflet/
